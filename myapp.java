package prenda;

import prenda.*;

import java.time.LocalDate;
	public class myapp {
		
		static void run() {
			prenda p= new prenda(10,"Algodon",20.5f,"Femenino","Otoo");
			
			System.out.println(p);
			
			p.addNvoLote(new lote(10,150,LocalDate.of(2021,10,10)));
			p.addNvoLote(new lote(12,200,LocalDate.of(2021, 10, 14)));
			
			System.out.println(p);
			
			System.out.println("Costo de produccion del lote="+p.recuperaLote(1).costoProduccion(p.getCostoProduccion()));
			System.out.println("Ganancia por lote"+p.recuperaLote(1).montoRecuperacionPorLote(p.getCostoProduccion()));
			System.out.println("Ganancia por pieza"+p.recuperaLote(1).montoRecuperacionPorPieza(p.getCostoProduccion()));
		    
			System.out.println("Costo de produccion del lote="+p.recuperaLote(0).costoProduccion(p.getCostoProduccion()));
			System.out.println("Ganancia por lote"+p.recuperaLote(0).montoRecuperacionPorLote(p.getCostoProduccion()));
			System.out.println("Ganancia por pieza"+p.recuperaLote(0).montoRecuperacionPorPieza(p.getCostoProduccion()));
		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
	        run();
		}

	}
