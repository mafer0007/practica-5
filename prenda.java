package prenda;

import java.util.ArrayList;
import prenda.*;

public class prenda {

	private int modelo;
	private String tela;
	private float costoProduccion;
	private String genero;
	private String temporada;
	private ArrayList<lote> lotes;

	public prenda(int modelo, String tela, float costoProduccion, String genero, String temporada) {
		super();
		this.modelo = modelo;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
		this.genero = genero;
		this.temporada = temporada;
		this.lotes = new ArrayList<lote>();
	}

	public void addNvoLote(lote lote) {
		lotes.add(lote);
	}

	public lote recuperaLote(int i) {
		lote l = null;
		if (this.lotes.size() > i)
			l = this.lotes.get(i);
		return l;
	}

	public int getModelo() {
		return modelo;
	}

	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public float getCostoProduccion() {
		return costoProduccion;
	}

	public void setCostoProduccion(float costoProduccion) {
		this.costoProduccion = costoProduccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) {
		this.temporada = temporada;
	}

	public ArrayList<lote> getLotes() {
		return lotes;
	}

	@Override
	public String toString() {
		return "Prenda [modelo=" + modelo + ", tela=" + tela + ", costoProduccion=" + costoProduccion + ", genero="
				+ genero + ", temporada=" + temporada + ", lotes=" + lotes + "]";
	}

}

